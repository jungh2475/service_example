package com.kewtea.example;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.kewtea.example.ctxlog.CtxLogger;


public class MainActivity extends ActionBarActivity {

    private Button btn_mainservice, btn_ctxlogger;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_mainservice=(Button) findViewById(R.id.start_service);
        btn_ctxlogger=(Button) findViewById(R.id.start_logger);
    }

    @Override
    protected void onResume() {

        serviceChck();
        super.onResume();
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

    //
    private void serviceChck(){

        if(isServiceRunning("com.kewtea.example.MainService")){btn_mainservice.setBackgroundColor(Color.GREEN);
        }else{btn_mainservice.setBackgroundColor(Color.LTGRAY);}
        if(isServiceRunning("com.kewtea.example.ctxlog.CtxLogger")){btn_ctxlogger.setBackgroundColor(Color.GREEN);
        }else{btn_ctxlogger.setBackgroundColor(Color.LTGRAY);}

    }
    public void onClickBtn(View btn){
        switch (btn.getId()){
            case R.id.start_service:
                //if -start or stopService
                if(!isServiceRunning("com.kewtea.example.MainService")){startService(new Intent(this,MainService.class));}
                else{stopService(new Intent(this,MainService.class));}

                break;
            case R.id.start_logger:
                //if -start or stopService
                if(!isServiceRunning("com.kewtea.example.ctxlog.CtxLogger")){startService(new Intent(this,CtxLogger.class));}
                else{stopService(new Intent(this,CtxLogger.class));}
                break;
            default:
                break;
        }
        serviceChck();
    }

    private boolean isServiceRunning(String serviceName) {
        ActivityManager manager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
