package com.kewtea.example.ctxlog;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by jungh on 4/11/15.
 */
public class CtxLogger extends Service {
    @Override
    public void onCreate() {

        Log.i("Base:", "CtxLoggerService created");
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Base:","CtxLoggerService onStartComand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.i("Base:","CtxLoggerService closed");
        stopSelf();
        super.onDestroy();
    }
}
